<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lead extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if(($this->session->userdata('userid') == null) || ($this->session->userdata('userid') == ""))
		{
			redirect(base_url());
		}
		/*else
		{
			redirect(base_url().'lead');
		}*/
	}
	public function index()
	{
		$this->load->view('header');
		$this->load->view('dashboard');
		//echo "userid:".$this->session->userdata('userid');
		$this->load->view('footer');
	}
	public function login()
	{
		//echo "hello World";
		$this->load->view('loginpage');
	}
}
?>