<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct()
	{
		parent::__construct();			
	}
	public function index()
	{
		$this->load->view('loginpage');
	}
	public function register()
	{
		$this->load->view('register');
	}
	public function checkLogin()
	{
		$email = $_POST['email'];
		$password = $_POST['password'];
		if($email != "" && $password != "")
		{
			$res = $this->leadmodel->checkLogin($email, $password);
			$rowCount = $res["rowCount"];
			$status = $res["status"];
			if($rowCount == 1 && $status == "active")
			{
				$data["isError"] = FALSE;
				$data["msg"] = "You Are Logged In Successfully.";
			}
			else
			{
				if($status == "inactive")
				{
					$data["isError"] = TRUE;
					$data["msg"] = "Your Account Has Been Suspended By Admin. Please Contact Admin.";
				}
				else
				{
					$data["isError"] = TRUE;
					$data["msg"] = "Email Or Password Is Not Matched.";
				}
			}
		}
		else
		{
			$data["isError"] = TRUE;
			$data["msg"] = "Please Fill All Details.";
		}
		echo json_encode($data);
	}
	public function logout()
	{
		$userData = array();	
		$this->session->set_userdata($userData);
		$this->session->sess_destroy();
		$this->load->helper('cookie');
		delete_cookie('ci_task');	
		redirect(base_url());
	}
}
?>