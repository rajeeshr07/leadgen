<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class leadmodel extends CI_Model
{

/*Pounraj Starts*/
	public function checkLogin($email, $password)
	{

	    $this->db->select('*');
	    $this->db->from('users');
	    $array = array('email' => $email, 'password' => md5($password), 'status' => 'active');
		$this->db->where($array); 
		$query = $this->db->get();
		$res = $query->result();
		$rows = $query->num_rows();
		if(!empty($res))
		{
			foreach($res as $row)
			{
				$status = $row->status;
				
				if($status == "active")
				{   
					$userData = array(
			    		'userid' => $row->id,
						'email' => $row->email,
						'firstname' => $row->firstname,
						'userteam' => $row->userteam,
			    		'status' => $row->status,
						'loggedin'=> TRUE
			    	);
					
					$this->session->set_userdata($userData);
				}
			}
		}
		
		$resArr["rowCount"] = $rows;
		$resArr["status"] = $status;
		return $resArr;
	}
}
?>