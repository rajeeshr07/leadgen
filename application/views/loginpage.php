<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
	<title>Digital Lead - Login Page</title>
	<!--favicon-->
	<link rel="icon" href="assets/images/favicon-32x32.png" type="image/png" />
	<!-- loader-->
	<link href="assets/css/pace.min.css" rel="stylesheet" />
	<script src="assets/js/pace.min.js"></script>
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css" />
	<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600&amp;family=Roboto&amp;display=swap" />
	<!-- Icons CSS -->
	<link rel="stylesheet" href="assets/css/icons.css" />
	<!-- App CSS -->
	<link rel="stylesheet" href="assets/css/app.css" />
</head>
<body class="bg-forgot">
	<!-- wrapper -->
	<div class="wrapper">
		<div class="authentication-forgot d-flex align-items-center justify-content-center">
			<div class="card shadow-lg forgot-box">
				<div class="card-body p-md-5">
					<div class="text-center">
						<img src="assets/images/icons/forgot-2.png" width="140" alt="" />
					</div>
					<form class="row g-3" id="loginForm" action="">
						<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
						<div class="col-12 mb-3 mt-4">
							<label for="inputEmailAddress" class="form-label">Email Address</label>
							<input type="email" class="form-control" id="inputEmailAddress" name="email" placeholder="Email Address">
						</div>
						<div class="col-12 mb-3">
							<label for="inputChoosePassword" class="form-label">Password</label>
							<div class="input-group" id="show_hide_password">
								<input type="password" class="form-control border-end-0" id="inputChoosePassword" value="" placeholder="Enter Password" name="password"> <a href="javascript:;" class="input-group-text bg-transparent"><i class="bx bx-hide"></i></a>
							</div>
						</div>
						<div class="d-grid gap-2">
							<button type="submit" class="btn btn-primary"><i class="bx bxs-lock-open"></i>Sign in</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- end wrapper -->
</body>
<!--plugins-->
<script src="assets/js/jquery.min.js"></script>
<!--Password show & hide js -->
<script>
	$(document).ready(function () {
		$("#show_hide_password a").on('click', function (event) {
			event.preventDefault();
			if ($('#show_hide_password input').attr("type") == "text") {
				$('#show_hide_password input').attr('type', 'password');
				$('#show_hide_password i').addClass("bx-hide");
				$('#show_hide_password i').removeClass("bx-show");
			} else if ($('#show_hide_password input').attr("type") == "password") {
				$('#show_hide_password input').attr('type', 'text');
				$('#show_hide_password i').removeClass("bx-hide");
				$('#show_hide_password i').addClass("bx-show");
			}
		});
	});
	$("#loginForm").submit(function(e)
	{
		e.preventDefault();
		$.ajax({
        	type: 'POST',
        	url:  'checkLogin',
        	data: $('#loginForm').serialize(),
        	success: function(data) {
				data = JSON.parse(data);
				var isError = data.isError;
				var msg = data.msg;
				var str = '';
				var redirectURL = '';
				if(isError)
				{
					str += '<div role="alert" class="alert alert-danger">';
					str += 'OOPS! ' + msg;
					str += '</div>';
				}
				else
				{
					str += '<div role="alert" class="alert alert-success">';
					str += 'WELL DONE! ' + msg;
					str += '</div>';
					redirectURL = '<?php echo base_url(); ?>lead/';
				}
				$("#responseMsg").html(str);
				if(!isError)
				{
					setTimeout(function()
					{
						location.href = redirectURL;
					},1000);
				}
			}
		})
	});
</script>
</html>